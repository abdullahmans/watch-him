package atmosphere.sh.watchhim.AdminApp.Fragments;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.github.channguyen.rsv.RangeSliderView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.squareup.picasso.Picasso;

import atmosphere.sh.watchhim.AdminApp.AdminMainActivity;
import atmosphere.sh.watchhim.AdminApp.ParentActivity;
import atmosphere.sh.watchhim.AdminApp.SupervisorActivity;
import atmosphere.sh.watchhim.MainActivity;
import atmosphere.sh.watchhim.Models.NotificationModel;
import atmosphere.sh.watchhim.Models.ParentModel;
import atmosphere.sh.watchhim.Models.SupervisorModel;
import atmosphere.sh.watchhim.R;
import de.hdodenhof.circleimageview.CircleImageView;

import static atmosphere.sh.watchhim.AdminApp.Fragments.AdminParentFragment.initShowOut;
import static atmosphere.sh.watchhim.AdminApp.Fragments.AdminParentFragment.rotateFab;
import static atmosphere.sh.watchhim.AdminApp.Fragments.AdminParentFragment.showIn;
import static atmosphere.sh.watchhim.AdminApp.Fragments.AdminParentFragment.showOut;

public class AdminSupervisorFragment extends Fragment
{
    View view;
    FloatingActionButton more_fab,add_supervisor_fab,logout_fab;

    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    DividerItemDecoration dividerItemDecoration;
    FirebaseRecyclerAdapter<SupervisorModel, supervisorsViewHolder> firebaseRecyclerAdapter;

    boolean rotate = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.admin_supervisor_fragment, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        more_fab = view.findViewById(R.id.more_fab);
        add_supervisor_fab = view.findViewById(R.id.add_supervisor_fab);
        logout_fab = view.findViewById(R.id.logout_fab);

        recyclerView = view.findViewById(R.id.recyclerview);

        initShowOut(add_supervisor_fab);
        initShowOut(logout_fab);

        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();
        databaseReference.keepSynced(true);

        layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        dividerItemDecoration = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(dividerItemDecoration);

        more_fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                rotate = rotateFab(v, !rotate);
                if (rotate) {
                    showIn(add_supervisor_fab);
                    showIn(logout_fab);
                } else {
                    showOut(add_supervisor_fab);
                    showOut(logout_fab);
                }
            }
        });

        add_supervisor_fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(getContext(), SupervisorActivity.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                {
                    startActivity(intent,
                            ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
                }
            }
        });

        logout_fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                FirebaseAuth.getInstance().signOut();

                updateUi();
            }
        });

        displaySupervisors();
    }

    private void displaySupervisors()
    {
        Query query = FirebaseDatabase.getInstance()
                .getReference()
                .child("Supervisors")
                .limitToLast(50);

        FirebaseRecyclerOptions<SupervisorModel> options =
                new FirebaseRecyclerOptions.Builder<SupervisorModel>()
                        .setQuery(query, SupervisorModel.class)
                        .build();

        firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<SupervisorModel, supervisorsViewHolder>(options)
        {
            @Override
            protected void onBindViewHolder(@NonNull final supervisorsViewHolder holder, int position, @NonNull final SupervisorModel model)
            {
                final String key = getRef(position).getKey();

                holder.name.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        NotificationModel notificationModel = new NotificationModel("Title","Hello World ...");

                        String s = databaseReference.child("Notification").child(key).push().getKey();
                        databaseReference.child("Notification").child(key).child(s).setValue(notificationModel);
                    }
                });

                holder.BindPlaces(model,getActivity());
            }

            @NonNull
            @Override
            public supervisorsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
            {
                View view = LayoutInflater.from(getContext()).inflate(R.layout.supervisor_item, parent, false);
                return new supervisorsViewHolder(view);
            }
        };
        recyclerView.setAdapter(firebaseRecyclerAdapter);
    }

    public static class supervisorsViewHolder extends RecyclerView.ViewHolder
    {
        TextView name,number;
        CircleImageView circleImageView;
        ProgressBar progressBar;
        RangeSliderView rangeSliderView;

        supervisorsViewHolder(View itemView)
        {
            super(itemView);

            name = itemView.findViewById(R.id.name_txt);
            number = itemView.findViewById(R.id.number_txt);
            circleImageView = itemView.findViewById(R.id.profile_pic);
            progressBar = itemView.findViewById(R.id.progressbar);
            rangeSliderView = itemView.findViewById(R.id.rsv_small);
        }

        void BindPlaces(final SupervisorModel supervisorModel, Activity acc)
        {
            rangeSliderView.setInitialIndex(6);
            rangeSliderView.setFilledColor(acc.getResources().getColor(R.color.colorAccent));

            name.setText(supervisorModel.getName());
            number.setText(supervisorModel.getMobilephone());

            if (supervisorModel.getImageurl().equals(""))
            {

            } else
                {
                    Picasso.get()
                            .load(supervisorModel.getImageurl())
                            .placeholder(R.drawable.ic_user1)
                            .error(R.drawable.ic_user1)
                            .into(circleImageView);
                }
        }
    }

    @Override
    public void onStart()
    {
        super.onStart();

        if (firebaseRecyclerAdapter != null)
        {
            firebaseRecyclerAdapter.startListening();
        }
    }

    @Override
    public void onStop()
    {
        super.onStop();

        if (firebaseRecyclerAdapter != null)
        {
            firebaseRecyclerAdapter.stopListening();
        }
    }

    public void updateUi ()
    {
        Intent intent = new Intent(getContext(), MainActivity.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            startActivity(intent,
                    ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
        }
    }
}
