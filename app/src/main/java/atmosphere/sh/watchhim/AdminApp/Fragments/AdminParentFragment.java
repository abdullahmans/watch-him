package atmosphere.sh.watchhim.AdminApp.Fragments;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.balysv.materialripple.MaterialRippleLayout;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import atmosphere.sh.watchhim.AdminApp.ParentActivity;
import atmosphere.sh.watchhim.MainActivity;
import atmosphere.sh.watchhim.Models.NotificationModel;
import atmosphere.sh.watchhim.Models.ParentModel;
import atmosphere.sh.watchhim.R;

public class AdminParentFragment extends Fragment
{
    View view;
    FloatingActionButton more_fab,add_parent_fab,logout_fab;

    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    FirebaseRecyclerAdapter<ParentModel, parentViewHolder> firebaseRecyclerAdapter;

    boolean rotate = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.admin_parent_fragment, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        more_fab = view.findViewById(R.id.more_fab);
        add_parent_fab = view.findViewById(R.id.add_parent_fab);
        logout_fab = view.findViewById(R.id.logout_fab);

        recyclerView = view.findViewById(R.id.recyclerview);

        initShowOut(add_parent_fab);
        initShowOut(logout_fab);

        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();
        databaseReference.keepSynced(true);

        layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);

        displayParent();

        more_fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                rotate = rotateFab(v, !rotate);
                if (rotate) {
                    showIn(add_parent_fab);
                    showIn(logout_fab);
                } else {
                    showOut(add_parent_fab);
                    showOut(logout_fab);
                }
            }
        });

        add_parent_fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(getContext(), ParentActivity.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                {
                    startActivity(intent,
                            ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
                }
            }
        });

        logout_fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                FirebaseAuth.getInstance().signOut();

                updateUi();
            }
        });
    }

    public void updateUi ()
    {
        Intent intent = new Intent(getContext(), MainActivity.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            startActivity(intent,
                    ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
        }
    }

    private void displayParent()
    {
        Query query = FirebaseDatabase.getInstance()
                .getReference()
                .child("Parents")
                .limitToLast(50);

        FirebaseRecyclerOptions<ParentModel> options =
                new FirebaseRecyclerOptions.Builder<ParentModel>()
                        .setQuery(query, ParentModel.class)
                        .build();

        firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<ParentModel, parentViewHolder>(options)
        {
            @Override
            protected void onBindViewHolder(@NonNull final parentViewHolder holder, int position, @NonNull final ParentModel model)
            {
                final String key = getRef(position).getKey();

                holder.name.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        NotificationModel notificationModel = new NotificationModel("Title","Hello World ...");

                        String s = databaseReference.child("Notification").child(key).push().getKey();
                        databaseReference.child("Notification").child(key).child(s).setValue(notificationModel);
                    }
                });

                holder.BindPlaces(model);
            }

            @NonNull
            @Override
            public parentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
            {
                View view = LayoutInflater.from(getContext()).inflate(R.layout.parent_item, parent, false);
                return new parentViewHolder(view);
            }
        };
        recyclerView.setAdapter(firebaseRecyclerAdapter);
    }

    public static class parentViewHolder extends RecyclerView.ViewHolder
    {
        TextView name;

        parentViewHolder(View itemView)
        {
            super(itemView);

            name = itemView.findViewById(R.id.name_txt);
        }

        void BindPlaces(final ParentModel parentModel)
        {
            name.setText(parentModel.getName());
        }
    }

    @Override
    public void onStart()
    {
        super.onStart();

        if (firebaseRecyclerAdapter != null)
        {
            firebaseRecyclerAdapter.startListening();
        }
    }

    @Override
    public void onStop()
    {
        super.onStop();

        if (firebaseRecyclerAdapter != null)
        {
            firebaseRecyclerAdapter.stopListening();
        }
    }

    public static void showIn(final View v) {
        v.setVisibility(View.VISIBLE);
        v.setAlpha(0f);
        v.setTranslationY(v.getHeight());
        v.animate()
                .setDuration(200)
                .translationY(0)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                    }
                })
                .alpha(1f)
                .start();
    }

    public static void initShowOut(final View v) {
        v.setVisibility(View.GONE);
        v.setTranslationY(v.getHeight());
        v.setAlpha(0f);
    }

    public static void showOut(final View v) {
        v.setVisibility(View.VISIBLE);
        v.setAlpha(1f);
        v.setTranslationY(0);
        v.animate()
                .setDuration(200)
                .translationY(v.getHeight())
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation)
                    {
                        v.setVisibility(View.GONE);
                        super.onAnimationEnd(animation);
                    }
                }).alpha(0f)
                .start();
    }

    public static boolean rotateFab(final View v, boolean rotate)
    {
        v.animate().setDuration(200)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                    }
                })
                .rotation(rotate ? 90f : 0f);
        return rotate;
    }
}
