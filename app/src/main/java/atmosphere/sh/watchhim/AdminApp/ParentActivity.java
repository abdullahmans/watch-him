package atmosphere.sh.watchhim.AdminApp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import atmosphere.sh.watchhim.MainActivity;
import atmosphere.sh.watchhim.Models.ParentModel;
import atmosphere.sh.watchhim.R;

public class ParentActivity extends AppCompatActivity
{
    EditText email_field,name_field,password_field,confirm_password_field;
    Button register;

    FirebaseAuth auth;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;

    ProgressDialog progressDialog;

    String uID;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent);

        auth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();

        email_field = findViewById(R.id.email_field);
        //name_field = findViewById(R.id.username_field);
        password_field = findViewById(R.id.password_field);
        confirm_password_field = findViewById(R.id.confirm_password_field);
        register = findViewById(R.id.register_btn);

        register.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String email = email_field.getText().toString();
                String name = name_field.getText().toString();
                String password = password_field.getText().toString();
                String confirmpassword = confirm_password_field.getText().toString();

                if (TextUtils.isEmpty(email))
                {
                    Toast.makeText(getApplicationContext(), "please enter parent email", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(name))
                {
                    Toast.makeText(getApplicationContext(), "please enter parent email", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (password.length() < 6)
                {
                    Toast.makeText(getApplicationContext(), "password is too short", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (!confirmpassword.equals(password))
                {
                    Toast.makeText(getApplicationContext(), "password is not matching", Toast.LENGTH_SHORT).show();
                    return;
                }

                progressDialog = new ProgressDialog(ParentActivity.this);
                progressDialog.setMessage("Please Wait Until Register ...");
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.show();
                progressDialog.setCancelable(false);

                createParent(email,name,password);
            }
        });
    }

    private void createParent(final String email, final String name, final String password)
    {
        auth.createUserWithEmailAndPassword(email,password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>()
                {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task)
                    {
                        if (task.isSuccessful())
                        {
                            uID = task.getResult().getUser().getUid();

                            FirebaseInstanceId.getInstance().getInstanceId()
                                    .addOnSuccessListener(new OnSuccessListener<InstanceIdResult>()
                                    {
                                        @Override
                                        public void onSuccess(InstanceIdResult instanceIdResult)
                                        {
                                            String token = instanceIdResult.getToken();

                                            databaseReference.child("Tokens").child("Parents").child(uID).setValue(token);
                                            saveParent(email,name);

                                            Intent intent = new Intent(getApplicationContext(), AdminMainActivity.class);
                                            startActivity(intent);
                                        }
                                    }).addOnFailureListener(new OnFailureListener()
                            {
                                @Override
                                public void onFailure(@NonNull Exception e)
                                {
                                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            });
                        } else
                            {
                                Toast.makeText(getApplicationContext(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                    }
                });
    }

    private void saveParent(String email, String name)
    {
        //ParentModel parentModel = new ParentModel(email,name);
        //databaseReference.child("Parents").child(uID).setValue(parentModel);
    }
}
