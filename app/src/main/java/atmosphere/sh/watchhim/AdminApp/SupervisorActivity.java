package atmosphere.sh.watchhim.AdminApp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import atmosphere.sh.watchhim.Models.SupervisorModel;
import atmosphere.sh.watchhim.R;
import de.hdodenhof.circleimageview.CircleImageView;

public class SupervisorActivity extends AppCompatActivity
{
    EditText email_field,first_field,middle_field,last_field,password_field,confirm_password_field,mobile_field,birth_field;
    RadioButton male_rb,female_rb;
    CircleImageView circleImageView;
    Button register_btn;

    String email,first,middle,last,password,confirmpassword,mobile,birth,gender,selectedimageurl;

    Uri photoPath;

    FirebaseAuth auth;

    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;

    FirebaseStorage firebaseStorage;
    StorageReference storageReference;

    ProgressDialog progressDialog;

    String uID;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_supervisor);

        email_field = findViewById(R.id.email_field);
        first_field = findViewById(R.id.firstname_field);
        middle_field = findViewById(R.id.middlename_field);
        last_field = findViewById(R.id.lastname_field);
        password_field = findViewById(R.id.password_field);
        confirm_password_field = findViewById(R.id.confirm_password_field);
        mobile_field = findViewById(R.id.mobilephone_field);
        birth_field = findViewById(R.id.birthdate_field);

        male_rb = findViewById(R.id.male_rb);
        female_rb = findViewById(R.id.female_rb);

        circleImageView = findViewById(R.id.profile_pic);

        register_btn = findViewById(R.id.register_btn);

        auth = FirebaseAuth.getInstance();

        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();
        databaseReference.keepSynced(true);

        firebaseStorage = FirebaseStorage.getInstance();
        storageReference = firebaseStorage.getReference().child("images");

        circleImageView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON_TOUCH)
                        .setAspectRatio(1,1)
                        .start(SupervisorActivity.this);
            }
        });

        male_rb.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                gender = "male";
            }
        });

        female_rb.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                gender = "female";
            }
        });

        register_btn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                email = email_field.getText().toString();
                first = first_field.getText().toString();
                middle = middle_field.getText().toString();
                last = last_field.getText().toString();
                password = password_field.getText().toString();
                confirmpassword = confirm_password_field.getText().toString();
                mobile = mobile_field.getText().toString();
                birth = birth_field.getText().toString();

                if (TextUtils.isEmpty(email))
                {
                    Toast.makeText(getApplicationContext(), "please enter email", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(first))
                {
                    Toast.makeText(getApplicationContext(), "please enter first name", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(middle))
                {
                    Toast.makeText(getApplicationContext(), "please enter middle name", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(last))
                {
                    Toast.makeText(getApplicationContext(), "please enter last name", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (password.length() < 6)
                {
                    Toast.makeText(getApplicationContext(), "password is too short", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (!confirmpassword.equals(password))
                {
                    Toast.makeText(getApplicationContext(), "password is not matching", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(gender))
                {
                    Toast.makeText(getApplicationContext(), "please select gender", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(mobile))
                {
                    Toast.makeText(getApplicationContext(), "please enter mobile number", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(birth))
                {
                    Toast.makeText(getApplicationContext(), "please enter birth date", Toast.LENGTH_SHORT).show();
                    return;
                }

                progressDialog = new ProgressDialog(SupervisorActivity.this);
                progressDialog.setMessage("Please Wait Until Register ...");
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.show();
                progressDialog.setCancelable(false);

                createSupervisorAccount(email,first + " " + middle + " " + last,password,gender,mobile,birth);
            }
        });
    }

    private void createSupervisorAccount(final String email, final String s, String password, final String gender, final String mobile, final String birth)
    {
        auth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>()
        {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task)
            {
                if (task.isSuccessful())
                {
                    uID = task.getResult().getUser().getUid();

                    if (photoPath == null)
                    {
                        addSupervisor(email,s,gender,mobile,birth,"",uID);
                    } else
                        {
                            uploadPic(email,s,gender,mobile,birth,uID);
                        }
                } else
                    {
                        Toast.makeText(getApplicationContext(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
            }
        });
    }

    private void uploadPic(final String email,final String full,final String gender, final String mobile, final String birth, final String id)
    {
        UploadTask uploadTask;

        final StorageReference ref = storageReference.child("images/" + photoPath.getLastPathSegment());

        uploadTask = ref.putFile(photoPath);

        Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception
            {
                if (!task.isSuccessful())
                {
                    throw task.getException();
                }

                // Continue with the task to get the download URL
                return ref.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>()
        {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onComplete(@NonNull Task<Uri> task)
            {
                Uri downloadUri = task.getResult();

                selectedimageurl = downloadUri.toString();

                addSupervisor(email,full,gender,mobile,birth,selectedimageurl,id);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception)
            {
                // Handle unsuccessful uploads
                Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    private void addSupervisor(String email, String full, String gender, String mobile, String birth, String selectedimageurl,String id)
    {
        SupervisorModel supervisorModel = new SupervisorModel(email,full,gender,mobile,birth,selectedimageurl);

        databaseReference.child("Supervisors").child(id).setValue(supervisorModel);

        FirebaseAuth.getInstance().signOut();

        Intent intent = new Intent(getApplicationContext(), AdminMainActivity.class);
        startActivity(intent);

        progressDialog.dismiss();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE)
        {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == Activity.RESULT_OK)
            {
                if (result != null)
                {
                    photoPath = result.getUri();

                    Picasso.get()
                            .load(photoPath)
                            .placeholder(R.drawable.ic_user2)
                            .error(R.drawable.ic_user2)
                            .into(circleImageView);
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE)
            {
                Exception error = result.getError();
            }
        }
    }
}