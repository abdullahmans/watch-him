package atmosphere.sh.watchhim.ParentApp;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.Arrays;
import java.util.List;

import atmosphere.sh.watchhim.AdminApp.AdminMainActivity;
import atmosphere.sh.watchhim.AdminApp.Fragments.AdminParentFragment;
import atmosphere.sh.watchhim.AdminApp.MapsActivity;
import atmosphere.sh.watchhim.AdminApp.ParentActivity;
import atmosphere.sh.watchhim.MainActivity;
import atmosphere.sh.watchhim.Models.ParentModel;
import atmosphere.sh.watchhim.Models.TokenModel;
import atmosphere.sh.watchhim.R;

import static atmosphere.sh.watchhim.AdminApp.Fragments.AdminParentFragment.initShowOut;
import static atmosphere.sh.watchhim.AdminApp.Fragments.AdminParentFragment.rotateFab;
import static atmosphere.sh.watchhim.AdminApp.Fragments.AdminParentFragment.showIn;
import static atmosphere.sh.watchhim.AdminApp.Fragments.AdminParentFragment.showOut;

public class ParentMainActivity extends AppCompatActivity
{
    FloatingActionButton more_fab,add_child_fab,logout_fab;

    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    //FirebaseRecyclerAdapter<ParentModel, AdminParentFragment.parentViewHolder> firebaseRecyclerAdapter;

    boolean rotate = false;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent_main);

        more_fab = findViewById(R.id.more_fab);
        add_child_fab = findViewById(R.id.add_child_fab);
        logout_fab = findViewById(R.id.logout_fab);

        recyclerView = findViewById(R.id.recyclerview);

        initShowOut(add_child_fab);
        initShowOut(logout_fab);

        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();
        databaseReference.keepSynced(true);

        layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);

        updateToken();

        more_fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                rotate = rotateFab(v, !rotate);
                if (rotate) {
                    showIn(add_child_fab);
                    showIn(logout_fab);
                } else {
                    showOut(add_child_fab);
                    showOut(logout_fab);
                }
            }
        });

        add_child_fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                {
                    startActivity(intent,
                            ActivityOptions.makeSceneTransitionAnimation(ParentMainActivity.this).toBundle());
                }
            }
        });

        logout_fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                FirebaseAuth.getInstance().signOut();

                updateUi();
            }
        });
    }

    public void updateToken ()
    {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnSuccessListener(new OnSuccessListener<InstanceIdResult>()
                {
                    @Override
                    public void onSuccess(InstanceIdResult instanceIdResult)
                    {
                        String uID = FirebaseAuth.getInstance().getCurrentUser().getUid();
                        String token = instanceIdResult.getToken();

                        TokenModel tokenModel = new TokenModel(token);

                        databaseReference.child("Tokens").child("Parents").child(uID).setValue(tokenModel);
                    }
                }).addOnFailureListener(new OnFailureListener()
        {
            @Override
            public void onFailure(@NonNull Exception e)
            {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void updateUi ()
    {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            startActivity(intent,
                    ActivityOptions.makeSceneTransitionAnimation(ParentMainActivity.this).toBundle());
        }
    }

    private long exitTime = 0;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void doExitApp() {
        if ((System.currentTimeMillis() - exitTime) > 2000)
        {
            Toast.makeText(this, "Press again to exit", Toast.LENGTH_SHORT).show();
            exitTime = System.currentTimeMillis();
        } else {
            finishAffinity();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBackPressed()
    {
        doExitApp();
    }
}
