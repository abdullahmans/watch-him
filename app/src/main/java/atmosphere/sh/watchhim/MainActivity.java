package atmosphere.sh.watchhim;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.transition.Fade;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import atmosphere.sh.watchhim.AdminApp.AdminMainActivity;
import atmosphere.sh.watchhim.AdminApp.MapsActivity;
import atmosphere.sh.watchhim.ParentApp.ParentMainActivity;

public class MainActivity extends AppCompatActivity
{
    EditText email_field,password_field;
    Button login_btn;

    String email,password,confirmpassword;

    FirebaseAuth auth;

    ProgressDialog progressDialog;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        auth = FirebaseAuth.getInstance();

        email_field = findViewById(R.id.email_field);
        password_field = findViewById(R.id.password_field);
        login_btn = findViewById(R.id.login_btn);

        login_btn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                email = email_field.getText().toString();
                password = password_field.getText().toString();

                if (TextUtils.isEmpty(email))
                {
                    Toast.makeText(getApplicationContext(), "please enter your email", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (password.length() < 6)
                {
                    Toast.makeText(getApplicationContext(), "password is too short", Toast.LENGTH_SHORT).show();
                    return;
                }

                progressDialog = new ProgressDialog(MainActivity.this);
                progressDialog.setMessage("Please Wait Until Login ...");
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.show();
                progressDialog.setCancelable(false);

                loginUser(email,password);
            }
        });
    }

    private void loginUser(String email, String password)
    {
        auth.signInWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>()
        {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task)
            {
                if (task.isSuccessful())
                {
                    if (task.getResult().getUser().getUid().equals("jqOHhdoqMcOd75d4cXsWou544Zq1"))
                    {
                        adminUi();
                    } else
                        {
                            parentUi();
                        }
                } else
                    {
                        Toast.makeText(getApplicationContext(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
            }
        });
    }

    public void adminUi ()
    {
        Intent intent = new Intent(getApplicationContext(), AdminMainActivity.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            startActivity(intent,
                    ActivityOptions.makeSceneTransitionAnimation(MainActivity.this).toBundle());
            progressDialog.dismiss();
        }
    }

    public void parentUi ()
    {
        Intent intent = new Intent(getApplicationContext(), ParentMainActivity.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            startActivity(intent,
                    ActivityOptions.makeSceneTransitionAnimation(MainActivity.this).toBundle());
            progressDialog.dismiss();
        }
    }

    @Override
    public void onBackPressed()
    {
        finishAffinity();
    }
}
