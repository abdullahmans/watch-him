package atmosphere.sh.watchhim.Models;

public class ParentModel
{
    private String email,name,gender,homephone,mobilephone,workphone,homeaddress,workaddress,jobtitle,socialstatus,educationalqual,birthdate;

    public ParentModel() {
    }

    public ParentModel(String email, String name, String gender, String homephone, String mobilephone, String workphone, String homeaddress, String workaddress, String jobtitle, String socialstatus, String educationalqual, String birthdate) {
        this.email = email;
        this.name = name;
        this.gender = gender;
        this.homephone = homephone;
        this.mobilephone = mobilephone;
        this.workphone = workphone;
        this.homeaddress = homeaddress;
        this.workaddress = workaddress;
        this.jobtitle = jobtitle;
        this.socialstatus = socialstatus;
        this.educationalqual = educationalqual;
        this.birthdate = birthdate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getHomephone() {
        return homephone;
    }

    public void setHomephone(String homephone) {
        this.homephone = homephone;
    }

    public String getMobilephone() {
        return mobilephone;
    }

    public void setMobilephone(String mobilephone) {
        this.mobilephone = mobilephone;
    }

    public String getWorkphone() {
        return workphone;
    }

    public void setWorkphone(String workphone) {
        this.workphone = workphone;
    }

    public String getHomeaddress() {
        return homeaddress;
    }

    public void setHomeaddress(String homeaddress) {
        this.homeaddress = homeaddress;
    }

    public String getWorkaddress() {
        return workaddress;
    }

    public void setWorkaddress(String workaddress) {
        this.workaddress = workaddress;
    }

    public String getJobtitle() {
        return jobtitle;
    }

    public void setJobtitle(String jobtitle) {
        this.jobtitle = jobtitle;
    }

    public String getSocialstatus() {
        return socialstatus;
    }

    public void setSocialstatus(String socialstatus) {
        this.socialstatus = socialstatus;
    }

    public String getEducationalqual() {
        return educationalqual;
    }

    public void setEducationalqual(String educationalqual) {
        this.educationalqual = educationalqual;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }
}
